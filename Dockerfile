FROM ubuntu:18.04
# Based on https://foldingathome.org/support/faq/installation-guides/linux/manual-installation-advanced/

# non interactive mode
ENV DEBIAN_FRONTEND=noninteractive

ARG MAJOR_VER
ARG VERSION

RUN apt-get update && apt-get install -y python python-gnome2 libx11-6 libgl1-mesa-glx freeglut3

#python-support
#ADD http://launchpadlibrarian.net/109052632/python-support_1.0.15_all.deb /python-support.deb
#RUN dpkg -i python-support.deb

COPY docker-entrypoint.sh /opt/entry/
COPY config.xml /etc/fahclient/

ADD https://download.foldingathome.org/releases/public/release/fahclient/debian-testing-64bit/${MAJOR_VER}/fahclient_${VERSION}_amd64.deb /fahclient.deb
ADD https://download.foldingathome.org/releases/public/release/fahcontrol/debian-testing-64bit/${MAJOR_VER}/fahcontrol_${VERSION}-1_all.deb ./fahcontrol.deb
ADD https://download.foldingathome.org/releases/public/release/fahviewer/debian-testing-64bit/${MAJOR_VER}/fahviewer_${VERSION}_amd64.deb ./fahviewer.deb

RUN dpkg -i --force-depends fahclient.deb
RUN dpkg -i --force-depends fahcontrol.deb
RUN dpkg -i --force-depends fahviewer.deb

# go to homedir
WORKDIR /var/lib/fahclient
# entrypoint
ENTRYPOINT ["/opt/entry/docker-entrypoint.sh"]